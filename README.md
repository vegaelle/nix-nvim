# Gaëlle’s Neovim configuration for Nix

This repository contains my configuration for the [Neovim](https://neovim.io)
text editor, ready to be applied on a [NixOS](https://nixos.org) system or any
POSIX system with the Nix package manager. It uses
[Home-manager](https://nix-community.github.io/home-manager/) to apply the
configuration to any user.

It depends on Neovim 0.5 or later.

## Contents

This setup suits my personal needs with Neovim, which are: development, mostly
with Python and Rust, script editing, and document writing (mails, various
markdown files…). It contains the following main plugins:

- solarized-dark theme (can be easily changed, but for now some colors are
    hardcoded)
- lspconfig: language server support, using the built-in LSP from Neovim 0.5,
    configured with Rust, Python, Bash, HTML, CSS, Javascript, and Lua.
- nvim-bufferline: buffers appearing as graphical tabs
- vim-web-devicons
- gitsigns: for git info column and commands
- galaxyline: maybe the quickest statusbar plugin, configured with Powerline
    separators
- vim-vsnip and friendly-snippets: not yet configured
- telescope and telescope-symbols: universal fuzzy finder
- truezen: a clean and focus-friendly writing mode
- tree-nvim: a file explorer sidebar, with colored icons, git and LSP status
%% - minimap: a nice code minimap with current section highlighted

## Screenshots

![Editing a Rust file](https://framagit.org/vegaelle/nix-nvim/-/raw/main/screenshots/rust.png)
![Completion popup](https://framagit.org/vegaelle/nix-nvim/-/raw/main/screenshots/completion.png)

## Usage

You need a working Nix or NixOS, and Home-Manager environment.

You can either activate this config in the global `configuration.nix` file (and
define per-user home-manager config) or in `home.nix` in your home directory.

For `configuration.nix`:

```nix
{ lib, config, pkgs, ... }:
let
  # […]
  nix-nvim = builtins.fetchGit {
    url = "https://framagit.org/vegaelle/nix-nvim.git";
    ref = "master";
  };
in
{
  imports = [
    # your existing imports
    <home-manager/nixos>
  ];
  home-manager.users.your_user = import "${nix-nvim}";
}
```

Then run `nixos-rebuild switch`, and `rehash` if your current shell is `zsh`.

For `home.nix`:

```nix
{ lib, config, pkgs, ... }:
let
  # […]
  nix-nvim = builtins.fetchGit {
    url = "https://framagit.org/vegaelle/nix-nvim.git";
    ref = "master";
  };
in
{
  imports = [ (import "${nix-nvim}") ];
}
```

Then run `home-manager switch`. There is no need for `rehash` even with `zsh`.
